﻿#include <iostream>

using namespace std;

//
class  Animal {
public:
    void virtual voice() {
        cout << "sound" << endl;
    }
};

class Dog : public Animal {
public:
    void  voice() {
        cout << "Woof!" << std::endl;
        
    }
};

class Cat : public Animal {
public:
    void voice() {
        cout << "Meow!" << std::endl;
    }
};

class Sheep : public Animal {
public:
    void voice() {
        cout << "Baa!" << std::endl;
    }
};

int main() {
    Animal* animals[3];
    animals[0] = new Dog;
    animals[1] = new Cat;
    animals[2] = new Sheep;

    for (int i = 0; i < 3; i++) {
        animals[i]->voice();
    }
    return 0;
    delete[] animals;
}

